<?php

namespace Database\Seeders;

use GuzzleHttp\Client;
use App\Models\domicile;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DomicileSeedersJogja extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $client = new Client();
        $url = 'https://api.binderbyte.com//wilayah/kabupaten?api_key=e17cdbe660d22b138a76413b03080c1efec779f2920355cc42860315bd65c6bc&id_provinsi=34';
        $response = $client->request('GET', $url);
        $content = $response->getBody()->getContents();
        $contenArray = json_decode($content, true);
        $meta['status code'] = $contenArray['code'];
        $meta['massage'] = $contenArray['messages'];
        $data = $contenArray['value'];
        $jogja = [];
        for ($i = 0; $i < count($data); $i++) {
            array_push($jogja, $data[$i]['name']);
        }
        for ($i = 0; $i < count($jogja); $i++) {
            domicile::create([
                'name' => $jogja[$i],
            ]);
        }
    }
}
