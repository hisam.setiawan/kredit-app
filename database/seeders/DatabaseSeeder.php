<?php

namespace Database\Seeders;

use App\Models\deposit;
use App\Models\kredit;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        for ($i = 0; $i < 20; $i++) {
            kredit::create([
                'name' => fake()->name(),
                'phone' => fake()->phoneNumber(),
                'email' => fake()->email(),
                'id_domicile' => fake()->numberBetween(1, 40),
                'address' => fake()->address(),
                'id_product' => fake()->numberBetween(12, 19),
                'nominal_placement' => fake()->numberBetween(100000, 5000000),
            ]);
        }
    }
}
