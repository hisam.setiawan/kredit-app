<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class deposit extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'deposits';
    protected $fillable = [
        'name',
        'phone',
        'email',
        'id_domicile',
        'nominal_placement'
    ];
}
