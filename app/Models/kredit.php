<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class kredit extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'kredits';
    protected $fillable = [
        'name',
        'phone',
        'email',
        'id_domicile',
        'address',
        'id_product',
        'nominal_placement'
    ];

    function product()
    {
        return $this->belongsTo(product::class, 'id_product', 'id');
    }
    function branch_office()
    {
        return $this->belongsTo(branch_office::class, 'id_branch', 'id');
    }
}
