<?php

namespace App\Http\Controllers;

use App\Models\kredit;
use App\Models\product;
use App\Models\domicile;
use App\Http\Resources\kreditResource;
use App\Http\Resources\productResource;
use App\Http\Resources\domicilesResource;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Symfony\Component\Console\Command\DumpCompletionCommand;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
