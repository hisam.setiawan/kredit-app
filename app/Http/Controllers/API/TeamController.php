<?php

namespace App\Http\Controllers\APi;

use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Http\Resources\teamResource;
use App\Traits\queryRepo;

class TeamController extends Controller
{
    use queryRepo;
    public function getTeamAll()
    {
        try {
            $message = 'get team data.';
            $komisaris = teamResource::collection($this->getTeamByType('komisaris'));
            $direksi = teamResource::collection($this->getTeamByType('direksi'));
            return ResponseFormatter::successTeam($komisaris, $direksi, $message);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return ResponseFormatter::error(null, $message, 500);
        }
    }
}
