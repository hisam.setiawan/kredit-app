<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Traits\rumusSimulasi;
use Illuminate\Support\Facades\Validator;

class SimulasiController extends Controller
{
    use rumusSimulasi;
    public function kreditSimulasi(Request $request)
    {
        try {
            $rules = [
                'jumlahPinjaman' => 'required',
                'lamaPinjaman' => 'required',
                'sukuBunga' => 'required',
                'mulaiPinjaman' => 'required',
                'tahun' => 'required',
                'perhitunganBunga' => 'required',
            ];

            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $message = $validator->errors();
                return ResponseFormatter::error(null, $message, 400);
            }
            $simulasi = $this->simulasiKredit($request->jumlahPinjaman, $request->sukuBunga, $request->lamaPinjaman, $request->mulaiPinjaman, $request->tahun, $request->perhitunganBunga);
            if ($simulasi['data'] == null) {
                $data = $simulasi['data'];
                $message = "data not found";
                return ResponseFormatter::error(null, $message, 400);
            }
            $data = $simulasi['data'];
            $detail = $simulasi['detail'];
            $message = "get simulasi data";
            return ResponseFormatter::successSimulasi($data, $detail, $message);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return ResponseFormatter::error(null, $message, 500);
        }
    }
    public function depositoSimulasi(Request $request)
    {
        try {
            $rules = [
                'depositAwal' => 'required',
                'tenorBulan' => 'required',
                'sukuBunga' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $message = $validator->errors();
                return ResponseFormatter::error(null, $message, 400);
            }
            $simulasi = $this->simulasiDeposito($request->depositAwal, $request->tenorBulan, $request->sukuBunga);
            $data = $simulasi;
            $message = "get simulasi data";
            return ResponseFormatter::success($data, $message);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return ResponseFormatter::error(null, $message, 500);
        }
    }
}
