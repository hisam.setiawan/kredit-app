<?php

namespace App\Http\Controllers\API;

use App\Traits\queryRepo;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;

class Branch_officeController extends Controller
{
    use queryRepo;
    public function getBranch_office()
    {
        try {
            $data = $this->getBranch_offcieAll();
            $message = 'get branch offcie data.';
            return ResponseFormatter::success($data, $message);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return ResponseFormatter::error(null, $message, 500);
        }
    }
    public function getContact()
    {
        try {

            $data = $this->getContactData();
            $message = 'get contac data.';
            return ResponseFormatter::success($data, $message);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return ResponseFormatter::error(null, $message, 500);
        }
    }
}
