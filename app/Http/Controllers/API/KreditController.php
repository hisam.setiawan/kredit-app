<?php

namespace App\Http\Controllers\API;

use App\Models\kredit;
use App\Traits\queryRepo;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class KreditController extends Controller
{
    use queryRepo;

    public function getTypeProduct(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), ["type" => "required"]);
            if ($validator->fails()) {
                $message = $validator->errors();
                return ResponseFormatter::error(null, $message, 400);
            }
            $data = $this->getAllProductBytype($request->type);
            $message = 'get product data.';
            return ResponseFormatter::success($data, $message);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return ResponseFormatter::error(null, $message, 500);
        }
    }
    public function postKreditData(Request $request)
    {
        try {
            $rules = [
                'name' => 'required',
                'phone' => 'required',
                'id_domicile' => 'required',
                'address' => 'required',
                'id_product' => 'required',
                'nominal_placement' => 'required',
            ];
            $validator =  Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $message = $validator->errors();
                return ResponseFormatter::error(null, $message, 400);
            }
            $product = $this->getDataProductById($request->id_product);
            kredit::create([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'id_domicile' => $request->id_domicile,
                'address' => $request->address,
                'id_product' => $product->id,
                'nominal_placement' => $request->nominal_placement,
            ]);

            $message = 'User kredit is created successfully.';
            return ResponseFormatter::success(null, $message);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return ResponseFormatter::error(null, $message, 500);
        }
    }
}
