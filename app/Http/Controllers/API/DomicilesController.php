<?php

namespace App\Http\Controllers\API;

use App\Traits\queryRepo;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\domicile;
use Illuminate\Http\Request;

class DomicilesController extends Controller
{
    use queryRepo;
    public function getDomicile()
    {
        try {
            $data = $this->getDomicileAll();
            $message = 'get domicile data.';
            return ResponseFormatter::success($data, $message);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return ResponseFormatter::error(null, $message, 500);
        }
    }
    public function destroy(Request $request)
    {
        try {
            $data = $this->getDomicileById($request->id);
            $data->delete();
            $message = 'succes delete data.';
            return ResponseFormatter::success($data, $message);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return ResponseFormatter::error(null, $message, 500);
        }
    }
    public function trash()
    {
        try {
            $data = $this->getDomicileTrash();
            $message = 'get trash data.';
            return ResponseFormatter::success($data, $message);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return ResponseFormatter::error(null, $message, 500);
        }
    }
    public function restore(Request $request)
    {
        try {
            $data = $this->getDomicileRestore($request->id);
            $message = 'succes restore data.';
            return ResponseFormatter::success($data, $message);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return ResponseFormatter::error(null, $message, 500);
        }
    }
}
