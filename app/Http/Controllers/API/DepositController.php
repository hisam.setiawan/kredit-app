<?php

namespace App\Http\Controllers\API;

use App\Models\deposit;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class DepositController extends Controller
{
    public function postDataDeposit(Request $request)
    {
        try {
            $rules = [
                'name' => 'required',
                'phone' => 'required',
                'id_domicile' => 'required',
                'nominal_placement' => 'required',
            ];
            $validator =  Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $message = $validator->errors();
                return ResponseFormatter::error(null, $message, 400);
            }
            deposit::create([
                'name' => $request->name,
                'phone' => $request->phone,
                'email' => $request->email,
                'id_domicile' => $request->id_domicile,
                'nominal_placement' => $request->nominal_placement,
            ]);

            $message = 'User deposit is created successfully.';
            return ResponseFormatter::success(null, $message);
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            return ResponseFormatter::error(null, $message, 500);
        }
    }
}
