<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class kreditResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
            'domicile' => $this->domicile,
            'address' => $this->address,
            'id_product' => $this->id_product,
            'nominal_placement' => $this->nominal_placement,
            'created_at' => Carbon::parse($this->created_at)->format('d-m-y')
        ];
    }
}
