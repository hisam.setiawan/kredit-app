<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class teamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $IMG_URL = config('global.img');
        if ($this->type == "komisaris") {
            return [
                'id' => $this->id,
                'fullname' => $this->fullname,
                'position' => $this->position,
                'image' => "$IMG_URL/$this->image.png",
                'created_at' => Carbon::parse($this->created_at)->format('d-m-y')
            ];
        }
        return [
            'id' => $this->id,
            'fullname' => $this->fullname,
            'position' => $this->position,
            'image' => "$IMG_URL/$this->image.jpg",
            'created_at' => Carbon::parse($this->created_at)->format('d-m-y')
        ];
    }
}
