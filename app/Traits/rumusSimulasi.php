<?php

namespace App\Traits;

trait rumusSimulasi
{
    public function hitungFlat($pinjaman, $bunga, $tenor)
    {
        $tahun = $tenor / 12;
        return ($pinjaman * ($bunga / 100) * $tahun) / $tenor;
    }
    public function hitungEfektif($pinjaman, $bunga)
    {
        return $pinjaman * ($bunga / 100) * (30 / 360);
    }
    public function hitungAnuitas($pinjaman, $bunga, $tenor)
    {
        $bunga_decimal = ($bunga / 100) / 12;
        return $pinjaman * $bunga_decimal * pow(1 + $bunga_decimal, $tenor) / (pow(1 + $bunga_decimal, $tenor) - 1);
    }


    public function simulasiKredit($pinjaman, $bunga, $tenor, $mulai, $tahun, $tipe)
    {
        $bungaBulanan = 0;
        $sisaPinjaman = $pinjaman;
        $angsuranPokok = $pinjaman / $tenor;
        $bulan =  [
            1 => "jan",
            2 => "feb",
            3 => "mar",
            4 => "apr",
            5 => "mei",
            6 => "jun",
            7 => "jul",
            8 => "ags",
            9 => "sep",
            10 => "okt",
            11 => "nov",
            12 => "des"
        ];
        if ($tipe == 'efektif') {
            $total_bunga = 0;
            $pokok = round($pinjaman / $tenor);

            for ($i = 0; $i <= $tenor; $i++) {
                if ($mulai > 12) {
                    $mulai = 1;
                    $tahun += 1;
                }
                if ($i != 0) {
                    $bungaBulanan = $this->hitungEfektif($sisaPinjaman, $bunga);
                    $total_bunga += $bungaBulanan;
                }
                $detail_simulasi[] = array(
                    "bulan" => "$bulan[$mulai] $tahun",
                    "bunga_bulanan" =>  $i == 0 ? 0 : round($bungaBulanan),
                    "pokok_bulanan" =>  $i == 0 ? 0 : round($angsuranPokok),
                    "total_angsuran" => $i == 0 ? 0 : round($angsuranPokok + $bungaBulanan),
                    "sisa_pinjaman" =>  $i == 0 ? $sisaPinjaman : round($sisaPinjaman -= $angsuranPokok)
                );
                $mulai++;
            }
            $data = [
                "angsuran" => $pokok,
                "jangka_waktu" => "$tenor bulan",
                "suku_bunga_pertahun" => "$bunga%",
                "tipe_bunga" => $tipe,
                "total_pinjaman" => round($pinjaman),
                "total_bunga" => round($total_bunga),
                "total_angsuran_pokok" => round($pokok * $tenor),
                "total_angsuran" => round($total_bunga + $pokok * $tenor)
            ];
            return ['detail' => $detail_simulasi, 'data' => $data,];
        } elseif ($tipe == 'flat') {
            $bungaBulanan = $this->hitungFlat($pinjaman, $bunga, $tenor);
            $pokok = $pinjaman / $tenor;
            for ($i = 0; $i <= $tenor; $i++) {
                if ($mulai > 12) {
                    $mulai = 1;
                    $tahun += 1;
                }
                $detail_simulasi[] = array(
                    "bulan" => "$bulan[$mulai] $tahun",
                    "bunga_bulanan" => $i == 0 ? 0 :  $bungaBulanan,
                    "pokok_bulanan" => $i == 0 ? 0 :  $angsuranPokok,
                    "total_angsuran" => $i == 0 ? 0 :  $angsuranPokok + $bungaBulanan,
                    "sisa_pinjaman" => $i == 0 ? $sisaPinjaman : $sisaPinjaman -= $angsuranPokok
                );
                $mulai++;
            }
            $data = [
                "angsuran" => $bungaBulanan + $pokok,
                "jangka_waktu" => "$tenor bulan",
                "suku_bunga_pertahun" => "$bunga%",
                "tipe_bunga" => $tipe,
                "total_pinjaman" => $pinjaman,
                "total_bunga" => $bungaBulanan * $tenor,
                "total_angsuran_pokok" => $pokok * $tenor,
                "total_angsuran" => ($bungaBulanan + $pokok) * $tenor
            ];
            return ['detail' => $detail_simulasi, 'data' => $data];
        } elseif ($tipe == "anuitas") {
            $angsuran = $this->hitungAnuitas($pinjaman, $bunga, $tenor);
            $totalBunga = 0;
            $totalPokok = 0;
            for ($i = 0; $i <= $tenor; $i++) {
                if ($mulai > 12) {
                    $mulai = 1;
                    $tahun += 1;
                }
                if ($i != 0) {
                    $bungaBulanan = $sisaPinjaman * ($bunga / 100) * (30 / 360);
                    $totalBunga += $bungaBulanan;
                    $pokokBulanan = $angsuran - $bungaBulanan;
                    $totalPokok += $pokokBulanan;
                    $sisaPinjaman -= $pokokBulanan;
                }
                $detail_simulasi[] = array(
                    "bulan" => "$bulan[$mulai] $tahun",
                    "bunga_bulanan" => $i == 0 ? 0 : round($bungaBulanan),
                    "pokok_bulanan" => $i == 0 ? 0 :  round($pokokBulanan),
                    "total_angsuran" => $i == 0 ? 0 :  round($bungaBulanan + $pokokBulanan),
                    "sisa_pinjaman" => $sisaPinjaman < 0 ? 0 : round($sisaPinjaman),
                );
                $mulai++;
            }
            $data = [
                "angsuran" => round($angsuran),
                "jangka_waktu" => "$tenor bulan",
                "suku_bunga_pertahun" => "$bunga%",
                "tipe_bunga" => $tipe,
                "total_pinjaman" => round($pinjaman),
                "total_bunga" => round($totalBunga),
                "total_angsuran_pokok" => round($totalPokok),
                "total_angsuran" => round($angsuran * $tenor)
            ];
            return ['detail' => $detail_simulasi, 'data' => $data];
        } else {
            return ['data' => null];
        }
    }

    public function simulasiDeposito($deposit, $tenor, $sukuBunga)
    {
        $bungaDesimal = $sukuBunga / 100;
        $pajak = 20 / 100;
        if ($deposit >= 7500000) {
            $bunga = $deposit * $bungaDesimal * $tenor / 12;
            $tarifPajak = $bunga * $pajak;
            $bungaPajak = $bunga - $tarifPajak;
            $depositAkhir = $deposit + $bungaPajak;
            $data = [
                'bunga' => round($bunga),
                'pajak' => round($tarifPajak),
                'bungaPajak' => round($bungaPajak),
                'depositAkhir' => round($depositAkhir)
            ];
            return $data;
        }
        $bunga = $deposit * $bungaDesimal * $tenor / 12;
        $depositAkhir = $deposit + $bunga;
        $bungaPajak = $bunga - 0;
        $data = [
            'bunga' => round($bunga),
            'pajak' => 0,
            'bungaPajak' => round($bungaPajak),
            'depositAkhir' => round($depositAkhir)
        ];
        return $data;
    }
}
