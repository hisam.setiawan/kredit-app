<?php

namespace App\Traits;

use App\Models\team;
use App\Models\kredit;
use App\Models\contact;
use App\Models\product;
use App\Models\domicile;
use App\Models\branch_office;
use App\Http\Resources\teamResource;
use App\Http\Resources\kreditResource;
use App\Http\Resources\contactResource;
use App\Http\Resources\productResource;
use App\Http\Resources\domicilesResource;
use App\Http\Resources\branch_officeResource;

trait queryRepo
{
    public function getKredituserById($id)
    {
        return new kreditResource(kredit::findOrFail($id));
    }

    public function getAllProductBytype($type)
    {
        return  productResource::collection(product::where('type', $type)->get());
    }
    public function getDataProductById($id)
    {
        return  product::where('id', $id)->first();
    }
    public function getTeamByType($type)
    {
        return teamResource::collection(team::where('type', $type)->get());
    }
    public function getDomicileAll()
    {
        return domicilesResource::collection(domicile::all());
    }
    public function getDomicileById($id)
    {
        return domicile::where('id', $id);
    }
    public function getDomicileTrash()
    {
        return domicilesResource::collection(domicile::onlyTrashed()->get());
    }
    public function getDomicileRestore($id)
    {
        return domicile::where('id', $id)->withTrashed()->restore();;
    }

    public function getBranch_offcieAll()
    {
        return branch_officeResource::collection(branch_office::orderBy('created_at', 'asc')->get());
    }

    public function getContactData()
    {
        return  contactResource::collection(contact::all());
    }
}
