<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\TeamController;
use App\Http\Controllers\API\KreditController;
use App\Http\Controllers\API\SimulasiController;
use App\Http\Controllers\API\TabunganController;
use App\Http\Controllers\API\DomicilesController;
use App\Http\Controllers\API\Branch_officeController;
use App\Http\Controllers\API\DepositController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
//Kredit app
Route::post('/kredit', [KreditController::class, 'postKreditData']);
Route::post('/kredit/simulasi', [SimulasiController::class, 'kreditSimulasi']);
//tabungan app
// Route::post('/tabungan', [TabunganController::class, 'postDataTabungan']);
//deposit app
Route::post('/deposit', [DepositController::class, 'postDataDeposit']);
Route::post('/deposit/simulasi', [SimulasiController::class, 'depositoSimulasi']);
//Get Master
Route::get('/product', [KreditController::class, 'getTypeProduct']);
Route::get('/team', [TeamController::class, 'getTeamAll']);
Route::get('/branch', [Branch_officeController::class, 'getBranch_office']);
Route::get('/contact', [Branch_officeController::class, 'getContact']);
Route::get('/domiciles', [DomicilesController::class, 'getDomicile']);
